import os
from retrying import retry

import boto3
import unittest

from fleet_control_service import SpotFleetService
from utility import suite_factory, case_factory


def get_ec2():
    boto3.setup_default_session(
        region_name=os.environ['AWS_REGION_NAME'],
        aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY']
    )

    ec2 = boto3.client('ec2')

    return ec2


def get_service(test_class_name):
    ec2 = get_ec2()

    iam_fleet_role = os.environ['IAM_FLEET_ROLE']
    fleet_instance_tag_name = os.environ.get('FLEET_INSTANCE_TAG_NAME', 'TESTING')
    image_id = os.environ.get('FLEET_IMAGE_ID')
    launch_template_name = os.environ.get('LAUNCH_TEMPLATE_NAME')
    launch_template_version = os.environ.get('LAUNCH_TEMPLATE_VERSION', '$Latest')
    valid_until_timedelta = {'hours': 0, 'minutes': 10}

    fleet_instance_tag_name = '{}_{}'.format(fleet_instance_tag_name, test_class_name)

    service = SpotFleetService(
        ec2, iam_fleet_role, fleet_instance_tag_name,
        launch_template_name=launch_template_name, launch_template_version=launch_template_version,
        image_id=image_id, valid_until_timedelta=valid_until_timedelta)

    return service


class AbstractTestSpotFleetService(unittest.TestCase):
    ec2 = None
    service = None

    @classmethod
    def setUpClass(cls):
        cls.ec2 = get_ec2()
        cls.service = get_service(cls.__name__)

    @classmethod
    def tearDownClass(cls):
        cls.service.terminate_all()


class TestWithoutFleet(AbstractTestSpotFleetService):
    def test_check_service_is_not_created(self):
        self.assertIsNone(self.service.current_fleet)

    def test_get_testing_launch_template(self):
        template = self.ec2.describe_launch_template_versions(
            LaunchTemplateName=self.service.launch_template_name, Versions=['$Latest'])['LaunchTemplateVersions'][0]

        self.assertIn('TagSpecifications', template['LaunchTemplateData'])
        self.assertIn(
            {'ResourceType': 'instance', 'Tags': [{'Key': self.service.fleet_instance_tag, 'Value': str(True)}]},
            template['LaunchTemplateData']['TagSpecifications']
        )


class TestZeroFleet(AbstractTestSpotFleetService):
    def test_check_zero_target_capacity(self):
        self.service.create_new_spot_fleet(0)

        current_fleet = self.service.current_fleet

        self.assertIsNotNone(current_fleet)
        self.assertEqual(current_fleet['SpotFleetRequestConfig']['TargetCapacity'], 0)
        self.assertEqual(current_fleet['SpotFleetRequestConfig']['OnDemandFulfilledCapacity'], 0)

    def test_try_run_two_fleets_simultaneously(self):
        with self.assertRaises(Exception):
            self.service.create_new_spot_fleet(0)

    def test_terminate_zero_fleet(self):
        current_fleet = self.service.current_fleet
        current_fleet_id = current_fleet['SpotFleetRequestId']

        self.assertIsNotNone(current_fleet)
        self.assertNotIn('cancelled', current_fleet['SpotFleetRequestState'])
        self.service.terminate_all()

        response = self.ec2.describe_spot_fleet_requests(SpotFleetRequestIds=[current_fleet_id])
        self.assertIn('cancelled', response['SpotFleetRequestConfigs'][0]['SpotFleetRequestState'])


class TestFleetLifecycle(AbstractTestSpotFleetService):
    def test_start_fleet(self):
        target_capacity = 1

        self.service.create_new_spot_fleet(target_capacity)
        current_fleet = self.service.current_fleet

        self.assertIsNotNone(current_fleet)
        self.assertIn(current_fleet['SpotFleetRequestState'], ['submitted', 'active'])

        template = current_fleet['SpotFleetRequestConfig']['LaunchTemplateConfigs'][0]['LaunchTemplateSpecification']
        self.assertEqual(template['LaunchTemplateName'], self.service.launch_template_name)
        self.assertEqual(template['Version'], str(self.service.launch_template_version))

        self.assertEqual(current_fleet['SpotFleetRequestConfig']['TargetCapacity'], target_capacity)
        self.assertEqual(current_fleet['SpotFleetRequestConfig']['OnDemandFulfilledCapacity'], 0)

        @retry(stop_max_attempt_number=20, wait_fixed=3000, retry_on_exception=lambda e: isinstance(e, AssertionError))
        def check():
            instances = self.service.current_running_instances
            self.assertEqual(len(instances), target_capacity)
            instance_ids = [instance['InstanceId'] for instance in instances]
            instance_statuses = self.ec2.describe_instance_status(InstanceIds=instance_ids, IncludeAllInstances=True)
            for instance in instance_statuses['InstanceStatuses']:
                self.assertIn(instance['InstanceState']['Name'], ['pending', 'running'])
        check()

    def test_modify_fleet(self):
        target_capacity = 2

        self.service.modify_fleet_target_capacity(target_capacity)

        self.assertIn(self.service.current_fleet['SpotFleetRequestState'], ['modifying'])

        @retry(stop_max_attempt_number=24, wait_fixed=5000, retry_on_exception=lambda e: isinstance(e, AssertionError))
        def check():
            instances = self.service.current_running_instances
            self.assertEqual(len(instances), target_capacity)
            instance_ids = [instance['InstanceId'] for instance in instances]
            instance_statuses = self.ec2.describe_instance_status(InstanceIds=instance_ids, IncludeAllInstances=True)
            for instance in instance_statuses['InstanceStatuses']:
                self.assertIn(instance['InstanceState']['Name'], ['pending', 'running'])

            current_fleet = self.service.current_fleet
            self.assertEqual(current_fleet['SpotFleetRequestConfig']['TargetCapacity'], target_capacity)
            self.assertEqual(current_fleet['SpotFleetRequestConfig']['OnDemandFulfilledCapacity'],  0)
        check()

    def test_stop_fleet(self):
        instances = self.service.current_running_instances
        instance_ids = [instance['InstanceId'] for instance in instances]

        self.service.terminate_all()

        @retry(stop_max_attempt_number=10, wait_fixed=2000, retry_on_exception=lambda e: isinstance(e, AssertionError))
        def check():
            instance_statuses = self.ec2.describe_instance_status(InstanceIds=instance_ids, IncludeAllInstances=True)
            for instance in instance_statuses['InstanceStatuses']:
                self.assertIn(instance['InstanceState']['Name'], ['shutting-down', 'terminated'])
        check()


# For PyCharm test runner
def load_tests(loader, tests, pattern):
    return suite_factory(*case_factory(globals().copy()))


if __name__ == '__main__':
    cases = suite_factory(*case_factory(globals().copy()))
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(cases)
