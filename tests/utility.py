import re
import unittest


def suite_factory(*test_cases, test_sorter=None, suite_maker=unittest.makeSuite, test_suite_class=unittest.TestSuite):
    """
    Make a test suite from test cases, or generate test suites from test cases.
    :param List | Tuple test_cases: TestCase subclasses to work on
    :param Callable test_sorter: sort tests using this function over sorting by line number
    :param suite_maker: should quacks like unittest.makeSuite
    :type test_suite_class: unittest.TestSuite
    :rtype: unittest.TestSuite
    """

    if test_sorter is None:
        ln = lambda f:    getattr(tc, f).__code__.co_firstlineno
        test_sorter = lambda a, b: ln(a) - ln(b)

    test_suite = test_suite_class()
    for tc in test_cases:
        test_suite.addTest(suite_maker(tc, sortUsing=test_sorter))

    return test_suite


def case_factory(scope=None,
                 case_sorter=lambda f: __import__("inspect").findsource(f)[1],
                 test_case_class=unittest.TestCase, test_case_regexp="^Test"):
    """
    :param dict scope: variables scope with tests
    :param Callable case_sorter: sort test cases using this function over sorting by line number
    :param test_case_class: superclass of test cases
    :param str test_case_regexp: regexp to match test cases
    :rtype: unittest.TestCase
    """

    return sorted(
        [scope[obj] for obj in scope if re.match(test_case_regexp, obj) and issubclass(scope[obj], test_case_class)],
        key=case_sorter
    )
