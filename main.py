import json
import os
import math
import time
import datetime

import boto3
import botocore.exceptions
from fleet_control_service import SpotFleetService


def main():
    import logging
    logging.basicConfig(format='%(asctime)s: %(message)s', level=logging.INFO, datefmt="%Y:%m:%d %H:%M:%S")
    logger = logging.getLogger(__name__)

    boto3.setup_default_session(
        region_name=os.environ['AWS_REGION_NAME'],
        aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY']
    )

    iam_fleet_role = os.environ['IAM_FLEET_ROLE']
    fleet_instance_tag_name = os.environ.get('FLEET_INSTANCE_TAG_NAME')
    image_id = os.environ.get('FLEET_IMAGE_ID')
    launch_template_name = os.environ.get('LAUNCH_TEMPLATE_NAME')
    launch_template_version = os.environ.get('LAUNCH_TEMPLATE_VERSION', '$Latest')
    valid_until_timedelta = {'hours': 0, 'minutes': 10}

    ec2 = boto3.client('ec2')
    service = SpotFleetService(
        ec2, iam_fleet_role, fleet_instance_tag_name,
        launch_template_name=launch_template_name, launch_template_version=launch_template_version,
        image_id=image_id, valid_until_timedelta=valid_until_timedelta
    )

    last_time_history_check = None

    while True and 1:
        with open('endpoint.json', 'r') as f:
            content = json.load(f)
        tasks_count = content['tasks_count']

        needed_instances = math.ceil(tasks_count / 2)

        if not service.current_fleet and needed_instances > 0:
            logger.info('>>>> Creating new spot fleet request with {} instances'.format(needed_instances))
            service.create_new_spot_fleet(needed_instances)

        target_capacity = service.current_target_capacity

        if needed_instances > target_capacity and service.current_fleet['SpotFleetRequestState'] != 'modifying':
            logger.info('>>>> Modify spot fleet size, old size {}, new size {}'
                        .format(target_capacity, needed_instances))
            service.modify_fleet_target_capacity(needed_instances)
        elif needed_instances == 0 and service.current_fleet:
            logger.info('>>>> Cancelling spot fleet request')
            service.terminate_all()

        running_instances_count = len(service.current_running_instances)
        instance_states = service.instance_states_table

        logger.info('Task count {}; currently needed instances {}; '.format(tasks_count, needed_instances))
        logger.info('Fleet target capacity {}; fleet instances count {}.'
                    .format(target_capacity, running_instances_count))
        logger.info('Instances states ({})'.format(
            ' '.join([
                key and '{}: {}'.format(key, instance_states.get(key)) or ' | '
                for key in
                ['running', None, 'pending', None, 'shutting-down', 'stopping', None, 'stopped', None, 'terminated']
            ])
        ))

        if service.current_fleet:
            if not last_time_history_check:
                last_time_history_check = service.current_fleet['CreateTime'] - datetime.timedelta(minutes=1)

            try:
                response = ec2.describe_spot_fleet_request_history(
                    SpotFleetRequestId=service.current_fleet_id,
                    StartTime=last_time_history_check.replace(microsecond=0)
                )
                last_time_history_check = response['LastEvaluatedTime']

                if response['HistoryRecords']:
                    logging.info("History:")
                    for history in response['HistoryRecords']:
                        msg = "AWS SFR EVENT::{} {}: {}".format(history['Timestamp'].replace(
                            microsecond=0), history['EventType'], str(history['EventInformation']))

                        if history['EventType'] == 'error':
                            logger.error(msg)
                        else:
                            logger.info(msg)
            except botocore.exceptions.ClientError:
                pass

        logger.info('')

        time.sleep(5)


if __name__ == '__main__':
    main()
