import datetime

import botocore.exceptions


class SpotFleetService:
    def __init__(self,
                 ec2_session, iam_fleet_role,
                 fleet_instance_tag_name,
                 launch_template_name, launch_template_version='$Latest',
                 valid_until_timedelta=None, image_id=None):
        """
            Fleet Control services differ by combination of LaunchTemplate and Fleet Instance Tag Name

            For make it possible to have several controlled fleets (ex. each one for different tasks),
            the combination of (Launch Template, Fleet Instance Tag) should be unique for each Service

            Fleet instance tag used for tagging instances that are being controlled by the Service.
            Is should be unique if there are two service controlled fleets for different task
        """

        self.ec2 = ec2_session
        self.IamFleetRole = iam_fleet_role

        self._fleet_instance_tag = fleet_instance_tag_name

        self.valid_until_timedelta = valid_until_timedelta if valid_until_timedelta else {'hours': 1}
        self.image_id = image_id

        self._base_launch_template_name, self._base_launch_template_version = \
            launch_template_name, launch_template_version
        self._launch_template_name, self._launch_template_version = \
            self._create_utility_launch_template(launch_template_name, launch_template_version)

    @property
    def launch_template_name(self):
        return self._launch_template_name

    @property
    def launch_template_version(self):
        return self._launch_template_version

    @property
    def fleet_instance_tag(self):
        return self._fleet_instance_tag

    def _create_utility_launch_template(self, base_launch_template_name, base_launch_template_version='$Latest'):
        base_template = self._get_template_version(base_launch_template_name, base_launch_template_version)

        utility_template_name = "{}_utility_{}".format(base_launch_template_name, self.fleet_instance_tag)
        try:
            old_utility_template = self._get_template_version(utility_template_name, '$Latest')
        except botocore.exceptions.ClientError:
            old_utility_template = None

        new_launch_template_data = base_template['LaunchTemplateData'].copy()
        if 'TagSpecifications' not in new_launch_template_data:
            new_launch_template_data['TagSpecifications'] = []
        new_launch_template_data['TagSpecifications'].extend([
            {
                'ResourceType': 'instance',
                'Tags': [{'Key': self.fleet_instance_tag, 'Value': str(True)}]
            },
        ])

        if self.image_id:
            new_launch_template_data['ImageId'] = self.image_id

        # create new version of utility template if there is not old template or if base template has changed
        if not old_utility_template:
            response = self.ec2.create_launch_template(
                LaunchTemplateName=utility_template_name,
                LaunchTemplateData=new_launch_template_data,
                VersionDescription="Created from {} ver.{}"
                                   .format(base_template['LaunchTemplateName'], base_template['VersionNumber'])
            )
        elif new_launch_template_data != old_utility_template['LaunchTemplateData']:
            response = self.ec2.create_launch_template_version(
                LaunchTemplateName=utility_template_name,
                LaunchTemplateData=new_launch_template_data,
                VersionDescription="Created from {} ver.{}"
                                   .format(base_template['LaunchTemplateName'], base_template['VersionNumber'])
            )
        else:
            return old_utility_template['LaunchTemplateName'], old_utility_template['VersionNumber']

        return utility_template_name, self._get_template_version(utility_template_name, '$Latest')['VersionNumber']

    def _get_template_version(self, launch_template_name, launch_template_version='$Latest'):
        template_version = self.ec2.describe_launch_template_versions(
            LaunchTemplateName=launch_template_name,
            Versions=[str(launch_template_version)]
        )['LaunchTemplateVersions'][0]

        return template_version

    @property
    def current_fleet(self):
        # get any non-cancelled instances which has the Service LaunchTemplate
        fleets = self.ec2.get_paginator('describe_spot_fleet_requests')\
                    .paginate().build_full_result()['SpotFleetRequestConfigs']
        non_cancelled_service_fleets = list(filter(
            lambda fleet: any(config['LaunchTemplateSpecification']['LaunchTemplateName'] == self.launch_template_name
                              for config in fleet['SpotFleetRequestConfig'].get('LaunchTemplateConfigs', [])),
            filter(lambda fleet: 'cancelled' not in fleet['SpotFleetRequestState'], fleets),
        ))

        return non_cancelled_service_fleets[0] if non_cancelled_service_fleets else None

    @property
    def current_fleet_id(self):
        return self.current_fleet['SpotFleetRequestId']

    @property
    def current_target_capacity(self):
        if not self.current_fleet:
            return 0

        return self.current_fleet['SpotFleetRequestConfig']['TargetCapacity']

    @property
    def current_running_instances(self):
        if not self.current_fleet:
            return []

        service_instances = self.ec2.get_paginator('describe_spot_fleet_instances').paginate(
            SpotFleetRequestId=self.current_fleet_id
        ).build_full_result()
        return service_instances['ActiveInstances']

    @property
    def instance_states_table(self):
        states = {'pending': 0, 'running': 0, 'shutting-down': 0, 'terminated': 0, 'stopping': 0, 'stopped': 0}

        # AWS API doesn't support use describe_instance_status with Filters directly, so firstly need to get IDs
        instances = self.ec2.get_paginator('describe_instances').paginate(
            Filters=[{"Name": 'tag:{}'.format(self.fleet_instance_tag), "Values": ['True']}]
        ).build_full_result()['Reservations']

        instance_ids = [instance['InstanceId'] for reservation in instances for instance in reservation['Instances']]
        if not instance_ids:
            return states
        service_instances_status = self.ec2.get_paginator('describe_instance_status').paginate(
            InstanceIds=instance_ids, IncludeAllInstances=True
        ).build_full_result()['InstanceStatuses']

        for instance in service_instances_status:
            states[instance['InstanceState']['Name']] += 1
        return states

    def create_new_spot_fleet(self, target_capacity):
        if self.current_fleet:
            raise Exception("This {} aleardy controls a spot fleet with template-tag {}"
                            .format(self.__class__.__name__, self.launch_template_name))

        response = self.ec2.request_spot_fleet(
            SpotFleetRequestConfig={
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': self.launch_template_name,
                        'Version': str(self.launch_template_version),
                    },
                }],

                'IamFleetRole': self.IamFleetRole,

                'TargetCapacity': target_capacity,
                'ValidUntil': datetime.datetime.utcnow().replace(microsecond=0) + datetime.timedelta(
                    **self.valid_until_timedelta),
                'TerminateInstancesWithExpiration': True,
            },
        )

        return response

    def modify_fleet_target_capacity(self, new_target_capacity):
        response = self.ec2.modify_spot_fleet_request(
            SpotFleetRequestId=self.current_fleet_id,
            TargetCapacity=new_target_capacity
        )

        return response

    def terminate_all(self):
        fleet = self.current_fleet
        if fleet:
            response = self.ec2.cancel_spot_fleet_requests(
                SpotFleetRequestIds=[fleet['SpotFleetRequestId']],
                TerminateInstances=True,
            )

            return response
