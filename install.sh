DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VENV_DIRECTORY_NAME="venv"
VENV_DIRECTORY_PATH="$DIR/$VENV_DIRECTORY_NAME"


# make venv directory if doesn't exist
if [ ! -d $VENV_DIRECTORY_PATH ]; then
	mkdir $VENV_DIRECTORY_PATH
	virtualenv -p python3.6 $VENV_DIRECTORY_PATH
fi


# Activate venv
source "$VENV_DIRECTORY_PATH/bin/activate"


# install requirements
pip install -r "$DIR/requirements.txt"
